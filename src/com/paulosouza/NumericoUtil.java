package com.paulosouza;

class NumericoUtil {
    static double minimoZeroMaximoUm(double valor) {
        return minimoZero(maximoUm(valor));
    }

    private static double minimoZero(double valor) {
        return Math.max(valor, 0d);
    }

    private static double maximoUm(double valor) {
        return Math.min(valor, 1d);
    }
}
