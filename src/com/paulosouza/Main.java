package com.paulosouza;

public class Main {

    public static void main(String[] args) {
	    Veiculo veiculoEletrico = new VeiculoEletrico();
	    Veiculo veiculoCombustao = new VeiculoCombustao();
	    Veiculo veiculoHibrido = new VeiculoHibrido();

        veiculoEletrico.acelera(1);
        veiculoEletrico.freia(1);

        veiculoCombustao.acelera(1);
        veiculoCombustao.freia(1);

        veiculoHibrido.acelera(1);
        veiculoHibrido.freia(1);
    }
}
