package com.paulosouza;

abstract class VeiculoGenerico implements Veiculo{
    private double velocidade;
    private double angulo;

    VeiculoGenerico () {
        this.velocidade = 0d;
    }

    @Override
    public void acelera(double taxa) {
        taxa = NumericoUtil.minimoZeroMaximoUm(taxa);

        injetaCombustivel(taxa);
    }

    @Override
    public void freia(double taxa) {
        taxa = NumericoUtil.minimoZeroMaximoUm(taxa);

        acionaFreios(taxa);
    }

    @Override
    public double getVelocidade() {
        return this.velocidade;
    }

    protected abstract void acionaFreios(double taxa);

    protected abstract void injetaCombustivel(double quantidade);
}
