package com.paulosouza;

public interface Veiculo {
    void acelera(double taxa);

    void freia(double taxa);

    double getVelocidade();
}
